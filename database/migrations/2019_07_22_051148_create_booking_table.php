<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string('username');
            $table->string('email');
            $table->string('phone');
            $table->unsignedBigInteger('movie_id')->index()->nullable();
            $table->date('date');
            $table->integer('book_seat');
            $table->string('book_token');
            $table->timestamps();

            $table->foreign('movie_id')->references('id')->on('movie');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking');
    }
}
