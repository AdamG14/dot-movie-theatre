<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Booking;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Carbon\Carbon;

$factory->define(Booking::class, function (Faker $faker) {
    $dateNow = Carbon::now()->format('Y-m-d');
    return [
        'username' => $faker->userName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'date' => $dateNow,
        'movie_id' => $faker->numberBetween(1, 5),
        'book_seat' => $faker->numberBetween(20, 50),
        'status' => 1,
        'book_token' => $faker->asciify('*****'),
    ];
});
