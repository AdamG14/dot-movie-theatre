<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function () {
    return [
        'username' => "adamghi_",
        'password' => '$2y$10$Yj4b/EzoGy0zPrTlK4gIXexauzCQD1qhvRfccCPvZ.6c7LUs9lpPW',
    ];
});
