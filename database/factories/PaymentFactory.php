<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Payment;
use Faker\Generator as Faker;

$factory->define(Payment::class, function (Faker $faker) {
    return [
        'price' => 30000,
        'return' => 0,
        'paid' => 30000,
        'booking_id' => $faker->numberBetween(1, 5),
    ];
});
