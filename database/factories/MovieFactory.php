<?php

use App\Movie;
use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(Movie::class, function (Faker $faker) {
    $startingDate = Carbon::now()->format('Y-m-d');
    // $startingDate = $faker->dateTimeBetween('this week', '+6 days');
    $endingDate = $faker->dateTimeBetween($startingDate, strtotime('+6 days'));
    return [
        'title' => $faker->realText(10),
        'description' => $faker->realText(200),
        'rate' => $faker->numberBetween(1, 10),
        'seats' => $faker->numberBetween(80, 120),
        'duration' => $faker->numberBetween(90, 140),
        'image' => 'imagemovie.png',
        'starting_date' => $startingDate,
        'ending_date' => $endingDate,
        'time' => $faker->time,
        'price' => 30000,
    ];
});
