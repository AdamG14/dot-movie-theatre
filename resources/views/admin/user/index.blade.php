@extends ('admin.master')
@section ('user.index')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Movies Data
      </h1>
      @if (Session::has('alert'))
      <div class="alert alert-success alert-dismissible" style="margin: 0px;">
          <a href="admin/movie"><button type="button" class="close" data-dismiss="alert">&times;</button></a>
          {{Session::get('alert')}}
      </div>
      @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <style>th{text-align: center;}</style>
                  <th>id</th>
                  <th>Username</th>
                  <th class="col-md-2">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($user as $u)
                <tr>
                  <td>{{ $u->id }}</td>
                  <td>{{ $u->username }}</td>
                  <td> <a style="margin-left: 20px;" href="/admin/user/edit/{{ $u->id }}" class="btn btn-warning">Edit</a>
                       <a href="/admin/user/delete/{{ $u->id }}" class="btn btn-danger" onClick="return confirm('Are you sure to delete this data?')">Delete</a>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection