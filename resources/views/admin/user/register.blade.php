@extends('admin.master')
@section('user.register')
<!-- Main content -->
<!-- general form elements -->
    <div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Create Admin</h3>
        <a href="/admin/movie" class="btn btn-primary" style="float: right;">Back</a>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form method="post" action="{{ url('admin/user/store') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="box-body">
                
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Username..." name="username">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Password..." name="password">
            </div>

        </div>
        <!-- /.box-body -->

        <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
@endsection