@extends('admin.master')
@section('movie.edit')
<!-- Main content -->
<!-- general form elements -->
    <div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Edit Account</h3>
        <a href="/admin/user" class="btn btn-primary" style="float: right;">Back</a>
        @if (Session::has('alert-danger'))
        <div class="alert alert-danger alert-dismissible" style="margin: 0px; margin-top: 14px;">
            <a href="admin/movie"><button type="button" class="close" data-dismiss="alert">&times;</button></a>
            {{Session::get('alert-danger')}}
        </div>
        @endif
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form method="post" action="update/{{ $user->id }}" enctype="multipart/form-data">
        {{ method_field('PUT')}}
        <div class="box-body">
                
            <div class="form-group">
                <input type="text" class="form-control" name="username" value="{{ $user->username }}">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="oldPassword" placeholder="Old Password">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="newPassword" placeholder="New Password">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="confirmation" placeholder="Retype New Password">
            </div>

        </div>
        <!-- /.box-body -->

        <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
@endsection