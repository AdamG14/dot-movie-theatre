<!-- Main Header -->
<header class="main-header" style="position: fixed; width: 100%;">

<!-- Logo -->
<a href="/admin/movie" class="logo">
  <span class="logo-lg"><b>Movie</b>Admin</span>
</a>

<!-- Header Navbar -->
<nav class="navbar navbar-static-top" role="navigation">
  <!-- Sidebar toggle button-->
  <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
    <span class="sr-only">Toggle navigation</span>
  </a>
  <a href="{{ url('auth/logout') }}" class="btn btn-danger" style="float: right; margin: 10px 10px;" onClick="return confirm('Apakah yakin untuk logout?')">Logout</a>
  </div>
</nav>
</header>