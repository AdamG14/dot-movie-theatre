<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar" style="position: fixed;">

<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar" >

  <!-- Sidebar Menu -->
  <ul class="sidebar-menu" data-widget="tree">
    
    <li class="header">MENU</li>
    <!-- Optionally, you can add icons to the links -->
    <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Movie</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/movie">Movie List</a></li>
            <li><a href="{{ route('movie.create') }}">Add New Movie</a></li>
          </ul>
    </li>
    <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Booking</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('admin/booking/') }}">Booking List</a></li>
            <li><a href="{{ url('admin/booking/verif') }}">Verification</a></li>
          </ul>
    </li>
    <li class="treeview">
          <a href="#"><i class="fa fa-link"></i> <span>Admin Account</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('admin/user') }}">Account List</a></li>
            <li><a href="{{ url('admin/user/register') }}">Create Account</a></li>
          </ul>
    </li>

  </ul>
  <!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->
</aside>
