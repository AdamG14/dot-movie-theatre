@extends ('admin.master')
@section ('booking.index')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bookings Data
      </h1>
      @if (Session::has('alert'))
      <div class="alert alert-success alert-dismissible" style="margin: 0px;">
          <a href="admin/movie"><button type="button" class="close" data-dismiss="alert">&times;</button></a>
          {{Session::get('alert')}}
      </div>
      @endif
      @if (Session::has('alert-danger'))
      <div class="alert alert-danger alert-dismissible" style="margin: 0px;">
          <a href="admin/movie"><button type="button" class="close" data-dismiss="alert">&times;</button></a>
          {{Session::get('alert-danger')}}
      </div>
      @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                <style>th{text-align: center;}</style>
                  <th>Id</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>Phone Number</th>
                  <th>Movie Title</th>
                  <th class="col-md-1">Date Time</th>
                  <th>Seats</th>
                  <th>Token</th>
                  <th>Status</th>
                  <th class="col-md-2">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($bookings as $b)
                <tr>
                  <td>{{ $b->id }}</td>
                  <td>{{ $b->username }}</td>
                  <td>{{ $b->email }}</td>
                  <td>{{ $b->phone }}</td>
                  <td>{{ $b->movie->title }}</td>
                  <td>{{ $b->date }} at {{ $b->movie->time }}</td>
                  <td>{{ $b->book_seat }}</td>
                  <td>{{ $b->book_token }}</td>
                  @if($b->status == 'pending')
                  <td style="background-color: red; color: white;">{{ $b->status }}</td>
                  @else
                  <td style="background-color: green; color: white;">{{ $b->status }}</td>
                  @endif
                  <td> <a href="/admin/booking/edit/{{ $b->id }}" class="btn btn-warning">Edit</a>
                       <a href="/admin/booking/delete/{{ $b->id }}" class="btn btn-danger" onClick="return confirm('Are you sure to delete this data?')">Delete</a>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection