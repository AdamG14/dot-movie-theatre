@extends ('admin.master')
@section ('booking.verif')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Token Verification
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <form action="{{ url('admin/booking/verif/check') }}" method="post">
                  {{ csrf_field() }}
              @foreach($booking as $b)
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Input token..." style="width: 93%;" name="token" value="{{ $b->book_token }}">
                    <button type="submit" class="btn btn-primary" style="float: right; margin-top: -34px;">Check</button>
                  </div>                  
              </form>
              <form action="{{ url('admin/booking/verif/return') }}" method="post">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <h1 style="text-align: center;">PAYMENT DETAIL</h1>
                
                  <div class="form-group" style="margin-top: 3%;">
                      <label for="" style="margin-left: 31%; margin-right: 1%; display: inline;">Price</label>
                      <input type="number" name="price" value="{{ $b->movie->price * $b->book_seat }}" class="form-control" 
                      style="width: 30%; display: inline;"readonly>
                  </div>                
                  <div class="form-group" >
                      <label for="" style="margin-left: 31%; margin-right: 2%; display: inline;">Pay</label>
                      <input type="number" name="paid" class="form-control" 
                      style="width: 30%; display: inline;">
                      <input type="number" name="bookId" value="{{ $b->id }}" hidden>
                      <button type="submit" class="btn btn-primary" style="float: right; margin-right: 27%;">Check</button>
                      @if($errors->has('paid'))
                      <div class="text-danger" style="margin-left: 35%;">
                          {{ $errors->first('paid')}}
                      </div>
                      @endif
                  </div>                
                  <div class="form-group">
                      <label for="" style="margin-left: 30%; margin-right: 1%; display: inline;">Return</label>
                      <input type="number" name="return" value="" class="form-control" 
                      style="width: 30%; display: inline;"readonly>
                  </div>  
                  @endforeach                
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection