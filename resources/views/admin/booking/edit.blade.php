@extends('admin.master')
@section('booking.edit')
<!-- Main content -->
<!-- general form elements -->
    <div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Edit Movie</h3>
        <a href="/admin/booking" class="btn btn-primary" style="float: right;">Back</a>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form method="post" action="update/{{ $bookings->id }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('PUT')}}
        <div class="box-body">
                
            <div class="form-group">
                <input type="text" class="form-control" name="username" value="{{ $bookings->username }}">
                @if($errors->has('username'))
                <div class="text-danger">
                    {{ $errors->first('username')}}
                </div>
                @endif
            </div>

            <div class="form-group">
                <input type="email" class="form-control" name="email" value="{{ $bookings->email }}">
                @if($errors->has('email'))
                <div class="text-danger">
                    {{ $errors->first('email')}}
                </div>
                @endif
            </div>

            <div class="form-group">
                <input type="text" class="form-control" name="seat" value="{{ $bookings->book_seat }}">
                @if($errors->has('seat'))
                <div class="text-danger">
                    {{ $errors->first('seat')}}
                </div>
                @endif
            </div>

            <div class="form-group">
                <input type="text" class="form-control" name="phone" value="{{ $bookings->phone }}">
                @if($errors->has('phone'))
                <div class="text-danger">
                    {{ $errors->first('phone')}}
                </div>
                @endif
            </div>

            <div class="form-group">
                <select name="title" class="form-control">
                    <option value="{{ $bookings->movie->id }}">{{ $bookings->movie->title }} at {{ $bookings->movie->time }}</option>

                    @foreach($movies as $m)
                    <option name="" value="{{ $m->id }}">{{ $m->title }} at {{ $m->time }}</option>
                    @endforeach

                </select>
                @if($errors->has('title'))
                <div class="text-danger">
                    {{ $errors->first('title')}}
                </div>
                @endif
            </div>

            <div class="form-group">
                <select name="date" class="form-control">
                    <option value=" {{ $bookings->date }}">{{ $bookings->date }}</option>

                    @foreach($dates as $d)
                    <option name="" value="{{ $d }}">{{ $d }}</option>
                    @endforeach

                </select>
                @if($errors->has('date'))
                <div class="text-danger">
                    {{ $errors->first('date')}}
                </div>
                @endif
            </div>


        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
@endsection