@extends ('admin.master')
@section ('booking.verif')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Token Verification
      </h1>
      @if (Session::has('alert-danger'))
      <div class="alert alert-danger alert-dismissible" style="margin: 0px;">
          <a href="admin/movie"><button type="button" class="close" data-dismiss="alert">&times;</button></a>
          {{Session::get('alert-danger')}}
      </div>
      @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <form action="{{ url('admin/booking/verif/check') }}" method="post">
              {{ csrf_field() }}
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Input token..." style="width: 93%;" name="token">
                      @if($errors->has('token'))
                      <div class="text-danger">
                          {{ $errors->first('token')}}
                      </div>
                      @endif
                    <button type="submit" class="btn btn-primary" style="float: right; margin-top: -34px;">Check</button>
                  </div>                  
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection