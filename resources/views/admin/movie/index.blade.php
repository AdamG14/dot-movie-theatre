@extends ('admin.master')
@section ('movie.index')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Movies Data
      </h1>
      @if (Session::has('alert'))
      <div class="alert alert-success alert-dismissible" style="margin: 0px;">
          <a href="admin/movie"><button type="button" class="close" data-dismiss="alert">&times;</button></a>
          {{Session::get('alert')}}
      </div>
      @endif
      @if (Session::has('alert-danger'))
      <div class="alert alert-danger alert-dismissible" style="margin: 0px;">
          <a href="admin/movie"><button type="button" class="close" data-dismiss="alert">&times;</button></a>
          {{Session::get('alert-danger')}}
      </div>
      @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <table id="example1" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <style>th{text-align: center;}</style>
                  <th>Id</th>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Rate</th>
                  <th>Price</th>                  
                  <th>Seats</th>
                  <th>Duration</th>
                  <th>Image</th>
                  <th>Starting Date</th>
                  <th>Ending Date</th>
                  <th>Time</th>
                  <th class="col-md-2">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($movies as $m)
                <tr>
                  <td>{{ $m->id }}</td>
                  <td>{{ $m->title }}</td>
                  <td>{{ $m->description }}</td>
                  <td>{{ $m->rate }}*</td>
                  <td>{{ number_format($m->price) }}</td>
                  <td>{{ $m->seats }}</td>
                  <td>{{ $m->duration }}</td>
                  <td><img src="{{ asset('/images/'. $m->image)}}" height="100px" width="100px" alt=""></td>
                  <td>{{ $m->starting_date }}</td>
                  <td>{{ $m->ending_date }}</td>
                  <td>{{ $m->time }}</td>
                  <td> <form action="{{ route('movie.destroy', $m->id) }}" method="post">{{ method_field('DELETE') }}{{ csrf_field() }}<a style="margin-left: 20px;" href="{{ route('movie.edit', $m->id) }}" class="btn btn-warning">Edit</a>
                       <button class="btn btn-danger" onClick="return confirm('Are you sure to delete this data?')">Delete</button></form>
                  </td>
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection