@extends('admin.master')
@section('movie.edit')
<!-- Main content -->
<!-- general form elements -->
    <div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Edit Movie</h3>
        <a href="/admin/movie" class="btn btn-primary" style="float: right;">Back</a>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form method="post" action="{{ route('movie.update', $movies->id) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('PUT')}}
        <div class="box-body">
                
            <div class="form-group">
                <input type="text" class="form-control" name="title" value="{{ $movies->title }}">
                @if($errors->has('title'))
                <div class="text-danger">
                    {{ $errors->first('title')}}
                </div>
                @endif
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="description" value="{{ $movies->description }}">
                @if($errors->has('description'))
                <div class="text-danger">
                    {{ $errors->first('description')}}
                </div>
                @endif
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="rate" value="{{ $movies->rate }}">
                @if($errors->has('rate'))
                <div class="text-danger">
                    {{ $errors->first('rate')}}
                </div>
                @endif
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="price" value="{{ $movies->price }}">
                @if($errors->has('price'))
                <div class="text-danger">
                    {{ $errors->first('price')}}
                </div>
                @endif
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="seats" value="{{ $movies->seats }}">
                @if($errors->has('seats'))
                <div class="text-danger">
                    {{ $errors->first('seats')}}
                </div>
                @endif
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="duration" value="{{ $movies->duration }}">
                @if($errors->has('duration'))
                <div class="text-danger">
                    {{ $errors->first('duration')}}
                </div>
                @endif
            </div>
            <div class="form-group">
                <input type="date" class="form-control" name="startingDate" value="{{ $movies->starting_date }}">
                @if($errors->has('startingDate'))
                <div class="text-danger">
                    {{ $errors->first('startingDate')}}
                </div>
                @endif
            </div>
            <div class="form-group">
                <input type="date" class="form-control" name="endingDate" value="{{ $movies->ending_date }}">
                @if($errors->has('endingDate'))
                <div class="text-danger">
                    {{ $errors->first('endingDate')}}
                </div>
                @endif
            </div>
            <div class="form-group">
                <input type="time" class="form-control" name="time" value="{{ $movies->time }}">
                @if($errors->has('time'))
                <div class="text-danger">
                    {{ $errors->first('time')}}
                </div>
                @endif
            </div>
            <div class="form-group">
                <label for="exampleInputFile">Current Image is {{ $movies->image }}</label>
                <input type="file" name="image">
                @if($errors->has('image'))
                <div class="text-danger">
                    {{ $errors->first('image')}}
                </div>
                @endif
            </div>

        </div>
        <!-- /.box-body -->

        <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
@endsection