<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>DOT-Movie</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-func.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<!-- START PAGE SOURCE -->
<div id="shell">
  <div id="header">
    <h1 style="margin: 20px 20px; position: absolute;">
      <a href="{{ url('user') }}" style="text-decoration: none; color: red; font-size: 50px;">DOT</a><a href="{{ url('user') }}" style="text-decoration: none; color: white; font-size: 50px;">-Movie</a>
    </h1>
    <div id="navigation">
      <ul>
        <li><a href="{{ url('user') }}">IN THEATERS</a></li>
        <li><a href="{{ url('user/upcoming') }}" class="active">COMING SOON</a></li>
      </ul>
    </div>
    <div id="sub-navigation">
      <ul>
        <li><a href="{{ url('user') }}">SHOW ALL</a></li>
      </ul>
    </div>
  </div>
    @if (Session::has('alert'))
        <div class="alert alert-danger alert-dismissible" style="width: 400px; margin-left: 30%;">
            {{Session::get('alert')}}
        <a href="{{ url('user/detail/'. $movies->id) }}"><button type="button" class="close" data-dismiss="alert">&times;</button></a>
    </div>
    @endif
    <div style="text align: center; margin-top: 20px;">
        <img class="img-responsive" src="{{ asset('/images/'. $movies->image)}}" alt="Image was not found in database" style="width: 30%; height: 47.5vh; float: right;">
        <h1 style="color: white;">{{$movies->title}}</h1>
        <p style="float: right; color: white; margin-top: -45px; margin-right: 10px; font-size: 12px; text-align: right;">Duration {{ $movies->duration }} minutes <br> Date {{ $movies->starting_date }} until {{ $movies->ending_date }} at {{ $movies->time }}</p><br><hr><br>
        <p style="margin-top: -8%; color: white; margin-bottom: 10%;">{{$movies->description}}</p>
    </div>
</div>
<!-- END PAGE SOURCE -->
</body>
</html>
