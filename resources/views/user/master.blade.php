<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>DOT-Movie</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-func.js"></script>

</head>
<body>
<!-- START PAGE SOURCE -->
<div id="shell">
  <div id="header">
    <h1 style="margin: 20px 20px; position: absolute;">
      <a href="{{ url('user') }}" style="text-decoration: none; color: #d91d2a; font-size: 50px;">DOT</a><a href="{{ url('user') }}" style="text-decoration: none; color: white; font-size: 50px;">-Movie</a>
    </h1>
    <div id="navigation">
      <ul>
        <li><a href="{{ url('user') }}" class="active">IN THEATERS</a></li>
        <li><a href="{{ url('user/upcoming') }}">COMING SOON</a></li>
      </ul>
    </div>
    <div id="sub-navigation">
      <ul>
        <li><a href="{{ url('user') }}">SHOW ALL</a></li>
        <li><a href="{{ url('user/rating') }}">TOP RATED</a></li>
        <li><a href="{{ url('user/views') }}">MOST VIEWED</a></li>
      </ul>
    </div>
  </div>
    @yield('homepage')
    @yield('detail')
</div>
<!-- END PAGE SOURCE -->
</body>
</html>