@extends('user.master')
@section('homepage')
@if (Session::has('alert'))
    <div class="notif">
        {{Session::get('alert')}}
        <a href="user"><button type="button" class="closeNotif" data-dismiss="alert">&times;</button></a>
    </div>
@endif
<div id="content">
    <div class="box">
        <div class="head">
        <h2>Top Rating</h2>
        <p class="text-right"><a href="#">See all</a></p>
        </div>
        @foreach($movies as $key => $m)
        @if (Carbon\Carbon::now()->format('Y-m-d') > Carbon\Carbon::parse($m->starting_date)->format('Y-m-d') 
        && 
        Carbon\Carbon::now()->format('Y-m-d') < Carbon\Carbon::parse($m->ending_date)->format('Y-m-d')
        ||
        Carbon\Carbon::now()->format('Y-m-d') == Carbon\Carbon::parse($m->starting_date)->format('Y-m-d')
        ||
        Carbon\Carbon::now()->format('Y-m-d') == Carbon\Carbon::parse($m->ending_date)->format('Y-m-d'))
            @if($m->rate > 5)
            <div class="movie">
            <div class="movie-image"><a href="{{ url('user/detail/'. $m->id)}}"><span class="name">{{ $m->title }}</span> <img src="{{ asset('/images/'. $m->image)}}" alt="" /></a> </div>
            <div class="rating">
                <p>Rate </p>
                <span class="comments">{{ $m->rate }}</span> </div>
            </div>
            @endif
        @endif
        @endforeach
    </div>
</div>
@endsection
