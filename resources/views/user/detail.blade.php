@extends('user.master')
@section('detail')
@if (Session::has('alert'))
<div class="alert alert-danger alert-dismissible" style="width: 400px; margin-left: 30%;">
{{Session::get('alert')}}
<a href="{{ url('user/detail/'. $movies->id) }}"><button type="button" class="close" data-dismiss="alert">&times;</button></a>
</div>
@endif
<div style="text align: center; margin-top: 20px;">
    <img class="img-responsive" src="{{ asset('/images/'. $movies->image)}}" alt="Image was not found in database" style="width: 30%; height: 47.5vh; float: right;">
    <h1 style="color: white;">{{$movies->title}}</h1>
    <p style="float: right; color: white; margin-top: -45px; margin-right: 10px; font-size: 12px; text-align: right;">Duration {{ $movies->duration }} minutes <br> Date {{ $movies->starting_date }} until {{ $movies->ending_date }} at {{ $movies->time }}</p><br><hr><br>
    <p style="margin-top: -8%; color: white; margin-bottom: 10%;">{{$movies->description}}</p>
    <a href="" data-toggle="modal" data-target="#modalContactForm" style="text-decoration: none; background-color: white; padding: 10px; color: black;">Book now!</a>
    
    <div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Book Movie</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body mx-3">
            <form method="post" action="{{ url('user/book/store') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" value="{{ $movies->id }}" name="movieId">
                    
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Full Name..." name="username">
                </div>

                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email..." name="email">
                </div>
                
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Phone Number..." name="phone">
                </div>

                <div class="form-group">
                    <label for="">Movie Title</label>
                    <input type="text" class="form-control" name="title" value="{{ $movies->title }}" readonly>
                </div>

                <div class="form-group">
                    <label for="">Show Time</label>
                    <input type="text" class="form-control" name="time" value="{{ $movies->time }}" readonly>
                </div>
            
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Seats..." name="seat">
                </div>

                <div class="form-group">
                    <select name="date" class="form-control">
                        <option value="">Choose Date...</option>

                        @foreach($dates as $keyD => $d)
                            @if(Carbon\Carbon::parse($d)->format('Y-m-d') > Carbon\Carbon::now()->format('Y-m-d')
                                ||
                                Carbon\Carbon::parse($d)->format('Y-m-d') == Carbon\Carbon::now()->format('Y-m-d'))  
                                <option name="" value="{{ $d }}">{{ $d }} 
                                @foreach($seats as $keyS => $s)
                                    @if($keyD == $keyS)
                                    available seat {{ $movies->seats - $s }}</option>
                                    @endif
                                @endforeach
                            @endif
                        @endforeach

                    </select>
                </div>

                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-unique">Book<i class="fas fa-paper-plane-o ml-1"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
@endsection