<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>DOT-Movie</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-func.js"></script>

</head>
<body>
<!-- START PAGE SOURCE -->
<div id="shell">
  <div id="header">
    <h1 style="margin: 20px 20px; position: absolute;">
      <a href="{{ url('user') }}" style="text-decoration: none; color: #d91d2a; font-size: 50px;">DOT</a><a href="{{ url('user') }}" style="text-decoration: none; color: white; font-size: 50px;">-Movie</a>
    </h1>
    <div id="navigation">
      <ul>
        <li><a href="{{ url('user') }}">IN THEATERS</a></li>
        <li><a href="{{ url('user/upcoming') }}" class="active">COMING SOON</a></li>
      </ul>
    </div>
    <div id="sub-navigation">
      <ul>
        <li><a href="{{ url('user') }}">SHOW ALL</a></li>
        <li><a href="{{ url('user/rating') }}">TOP RATED</a></li>
        <li><a href="{{ url('user/views') }}">MOST VIEWED</a></li>
      </ul>
    </div>
  </div>
    @if (Session::has('alert'))
        <div class="notif">
            {{Session::get('alert')}}
            <a href="user"><button type="button" class="closeNotif" data-dismiss="alert">&times;</button></a>
        </div>
    @endif
    <div id="content">
        <div class="box">
            <div class="head">
            <h2>Coming Soon</h2>
            <p class="text-right"><a href="#">See all</a></p>
            </div>
            @foreach($movies as $m)
            @if(Carbon\Carbon::now()->format('Y-m-d') < Carbon\Carbon::parse($m->starting_date)->format('Y-m-d'))
            <div class="movie">
            <div class="movie-image"><a href="{{ url('user/detail2/'. $m->id)}}"><span class="name">{{ $m->title }}</span> <img src="{{ asset('/images/'. $m->image)}}" alt="" /></a> </div>
            <div class="rating">
                <p>Playing Date</p>
                <span class="comments">{{ $m->starting_date }}</span> </div>
            </div>
            @endif
            @endforeach
        </div>
    </div>
</div>
<!-- END PAGE SOURCE -->
</body>
</html>
