@component('mail::message')
Dear, {{$email}} <br><br>
Hello **{{$username}}**,  
Here's your booking detail<br><br>
Username        : {{$username}}<br>
Email           : {{$email}}<br>
Phone Number    : {{$phone}}<br>
Movie           : {{$movie}}<br>
Date            : {{$date}}<br>
Time            : {{$time}}<br>
Booked Seat     : {{$seat}}<br>
Token           : {{$token}}<br><br>
Sincerely,
DOT-Movie team.
@endcomponent