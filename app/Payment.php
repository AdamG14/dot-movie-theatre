<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payment';
    protected $fillable = ['price', 'return', 'paid', 'booking_id'];
    public function booking()
    {
        return $this->belongsTo('App\Booking', 'booking_id', 'id');
    }
}
