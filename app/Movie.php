<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movie extends Model
{
    use SoftDeletes;

    protected $table = 'movie';
    protected $fillable = ['title', 'description', 'rate', 'seats', 'duration', 'starting_date', 'ending_date', 'time', 'image', 'price'];
}
