<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingNotification extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($payload)
    {
        $this->data = $payload;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('Admin@movie-dot.co.id', 'Mailtrap')
        ->subject('Booking Confirmation')
        ->markdown('mails.confirm')
        ->with([
            'email'     => $this->data['email'],
            'username'  => $this->data['username'],
            'phone'     => $this->data['phone'],
            'movie'     => $this->data['movie'],
            'time'      => $this->data['time'],
            'seat'      => $this->data['seat'],
            'date'      => $this->data['date'],
            'token'     => $this->data['token'],
        ]);
    }
}
