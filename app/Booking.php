<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use SoftDeletes;
    protected $table = 'booking';

    protected $fillable = ['username', 'email', 'phone', 'movie_id', 'date', 'book_seat', 'book_token', 'status'];
    protected $dates = ['deleted_at'];
    public function movie()
    {
        return $this->belongsTo('App\Movie', 'movie_id', 'id')->withTrashed();
    }
}
