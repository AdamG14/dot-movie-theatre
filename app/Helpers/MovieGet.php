<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use App\Movie;

class MovieGet {
    public static function getMovie($id) {
        $movies = Movie::find($id);
        return $movies;
    }
}