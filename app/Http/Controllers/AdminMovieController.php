<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Helpers\MovieGet;
use App\Movie;
use App\User;
use App\Booking;
use App\Payment;
use Carbon\Carbon;

class AdminMovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = Movie::orderBy('id', 'DESC')->get();
        return view('admin.movie.index', ['movies' => $movies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.movie.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validasi data dari form
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'rate' => 'required',
            'price' => 'required',
            'seats' => 'required',
            'duration' => 'required',
            'startingDate' => 'required',
            'endingDate' => 'required',
            'time' => 'required',
            'image' => 'required',
        ]);
        
        // Memasukan data yang sudah tervalidasi
        Movie::create([
            'title' => $request->title,
            'description' => $request->description,
            'rate' => $request->rate,
            'price' => $request->price,
            'seats' => $request->seats,
            'duration' => $request->duration,
            'starting_date' => $request->startingDate,
            'ending_date' => $request->endingDate,
            'time' => $request->time,
            'image' => $request->image->getClientOriginalName(),
        ]);

        $image = $request->file('image');
        $target = '/public/images';
        $image->move(\base_path() . $target, $image->getClientOriginalName());
        
        Session::flash('alert', 'Sukses tambah movie');
        return redirect('/admin/movie');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $movies = MovieGet::getMovie($id);
        return view('admin.movie.edit', ['movies' => $movies]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $movies = Movie::find($id);

        // Validasi data dari form
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'rate' => 'required',
            'price' => 'required',
            'seats' => 'required',
            'duration' => 'required',
            'startingDate' => 'required',
            'endingDate' => 'required',
            'time' => 'required',
        ]);

        if($request->image == null) {
            $imageName = $movies->image;
        } else {
            $imageName = $request->image->getClientOriginalName();
            $image = $request->file('image');
            $target = '/public/images';
            $image->move(\base_path() . $target, $imageName);
        }
        
        $movies->title = $request->title;
        $movies->description = $request->description;
        $movies->rate = $request->rate;
        $movies->price = $request->price;
        $movies->seats = $request->seats;
        $movies->duration = $request->duration;
        $movies->starting_date = $request->startingDate;
        $movies->ending_date = $request->endingDate;
        $movies->time = $request->time;
        $movies->image = $imageName;
        $movies->save();
        
        Session::flash('alert', 'Sukses update movie');
        return redirect('admin/movie'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $movie = Movie::find($id);
        $booking = Booking::where('movie_id', $id)->where('status', 1)->get();
        $checker = count($booking);

        if($checker == 0) {
            $movie->delete();
        
            Session::flash('alert', 'Success remove movie');
            return redirect('/admin/movie');
        } else {
            Session::flash('alert-danger', 'Cant remove movie, theres still booking pending');
            return redirect('/admin/movie');
        }
    }
}
