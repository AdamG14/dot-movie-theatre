<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Movie;
use App\User;
use App\Booking;
use App\Payment;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function indexMovie()
    {
        $movies = Movie::orderBy('id', 'DESC')->get();
        return view('admin.movie.index', ['movies' => $movies]);
    }

    public function createMovie()
    {
        return view('admin.movie.create');
    }

    public function storeMovie(Request $request)
    {
        // Validasi data dari form
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'rate' => 'required',
            'price' => 'required',
            'seats' => 'required',
            'duration' => 'required',
            'startingDate' => 'required',
            'endingDate' => 'required',
            'time' => 'required',
            'image' => 'required',
        ]);
        
        // Memasukan data yang sudah tervalidasi
        Movie::create([
            'title' => $request->title,
            'description' => $request->description,
            'rate' => $request->rate,
            'price' => $request->price,
            'seats' => $request->seats,
            'duration' => $request->duration,
            'starting_date' => $request->startingDate,
            'ending_date' => $request->endingDate,
            'time' => $request->time,
            'image' => $request->image->getClientOriginalName(),
        ]);

        $image = $request->file('image');
        $target = '/public/images';
        $image->move(\base_path() . $target, $image->getClientOriginalName());
        
        Session::flash('alert', 'Sukses tambah movie');
        return redirect('/admin/movie');
    }

    public function editMovie($id)
    {
        $movies = Movie::find($id);
        return view('admin.movie.edit', ['movies' => $movies]);
    }

    public function updateMovie(Request $request, $id)
    {
        $movies = Movie::find($id);

        // Validasi data dari form
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'rate' => 'required',
            'price' => 'required',
            'seats' => 'required',
            'duration' => 'required',
            'startingDate' => 'required',
            'endingDate' => 'required',
            'time' => 'required',
        ]);

        if($request->image == null) {
            $imageName = $movies->image;
        } else {
            $imageName = $request->image->getClientOriginalName();
            $image = $request->file('image');
            $target = '/public/images';
            $image->move(\base_path() . $target, $imageName);
        }
        
        $movies->title = $request->title;
        $movies->description = $request->description;
        $movies->rate = $request->rate;
        $movies->price = $request->price;
        $movies->seats = $request->seats;
        $movies->duration = $request->duration;
        $movies->starting_date = $request->startingDate;
        $movies->ending_date = $request->endingDate;
        $movies->time = $request->time;
        $movies->image = $imageName;
        $movies->save();
        
        Session::flash('alert', 'Sukses update movie');
        return redirect('admin/movie'); 
    }

    public function destroyMovie($id)
    {
        $movie = Movie::find($id);
        $booking = Booking::where('movie_id', $id)->where('status', 1)->get();
        $checker = count($booking);

        if($checker == 0) {
            $movie->delete();
        
            Session::flash('alert', 'Success remove movie');
            return redirect('/admin/movie');
        } else {
            Session::flash('alert-danger', 'Cant remove movie, theres still booking pending');
            return redirect('/admin/movie');
        }
    }

    public function indexBooking()
    {
        $bookings = Booking::orderBy('id', 'DESC')->get();
        return view('admin.booking.index', ['bookings' => $bookings]);
    }

    public function editBooking($id)
    {
        $bookings = Booking::find($id);
        $movies = Movie::all();
        // Get dates between starting date and ending date
        $from = Carbon::createFromDate($bookings->movie->ending_date);
        $to = Carbon::createFromDate($bookings->movie->starting_date);
        $dates = $this->generateDateRange($to, $from);

        return view('admin.booking.edit', ['bookings' => $bookings, 'movies' => $movies, 'dates' => $dates]);
    }

    private function generateDateRange(Carbon $start_date, Carbon $end_date)
    {
        $dates = [];
        for($date = $start_date; $date->lte($end_date); $date->addDay()) {
            $dates[] = $date->format('Y-m-d');
        }
        return $dates;
    }

    public function updateBooking(Request $request, $id)
    {
        // Validasi data dari form
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'title' => 'required',
            'seat' => 'required',
            'date' => 'required',
        ]);

        $bookings = Booking::find($id);
        $bookings->username = $request->username;
        $bookings->email = $request->email;
        $bookings->phone = $request->phone;
        $bookings->movie_id = $request->title;
        $bookings->book_seat = $request->seat;
        $bookings->date = $request->date;
        $bookings->save();

        Session::flash('alert', 'Success update book');
        return redirect('admin/booking'); 
    }

    public function destroyBooking($id)
    {
        $booking = Booking::find($id);

        if($booking->status == 'pending') {
            $booking->delete();
        
            Session::flash('alert', 'Success remove booking');
            return redirect('/admin/booking');
        } else {
            Session::flash('alert-danger', 'Booking has paid, cant delete');
            return redirect('/admin/booking');
        }
    }

    public function indexUser()
    {
        $user = User::orderBy('id', 'DESC')->get();
        return view('admin.user.index', ['user' => $user]);
    }

    public function createUser()
    {
        return view('admin.user.register');
    }

    public function storeUser(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);   
        
        $data = new User();
        $data->username = $request->username;
        $data->password = bcrypt($request->password);
        $data->save();
        
        Session::flash('alert', 'Sukses tambah admin');
        return redirect('/admin/user');
    }

    public function editUser($id)
    {
        $user = User::find($id);
        return view('admin.user.edit', ['user' => $user]);
    }

    public function updateUser(Request $request, $id)
    {
        $user = User::find($id);
        $password = User::where('id', $id)->get('password');
        
        $this->validate($request, [
            'username' => 'required',
            'oldPassword' => 'required',
            'newPassword' => 'required|required_with:confirmation|same:confirmation',
            'confirmation' => 'required',
        ]);

        $password = $password[0]->password;
        
        if(Hash::check($request->oldPassword, $password)) {
            
            $user->username = $request->username;
            $user->password = bcrypt($request->newPassword);
            $user->save();

            Session::flash('alert', 'Succes change account credentials');
            return redirect('admin/user'); 

        } else {

            Session::flash('alert-danger', 'Wrong password!');
            return redirect('admin/user/edit/'. $id); 
            
        }
    }
    
    public function destroyUser($id)
    {
        $user = User::find($id);
        $user->delete();
    
        Session::flash('alert', 'Success remove account');
        return redirect('/admin/user');
    }

    public function indexVerif()
    {   
        return view('admin.booking.verification');
    }
    
    public function checkToken(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
        ]);

        $booking = Booking::where('book_token', $request->token)->get();

        if(!$booking->isEmpty()) {

            $checkStatus = Booking::where('book_token', $request->token)->get('status');

            if($checkStatus[0]->status == 'paid') {

                Session::flash('alert-danger', 'This booking has paid');
                return redirect('/admin/booking/verif');

            } else {

                return redirect('admin/booking/verif/index/'. $booking[0]->id);

            }
        } else {

            Session::flash('alert-danger', 'Token not found');
            return redirect('/admin/booking/verif');

        }
    }

    public function indexCheck($id)
    {
        $booking = Booking::where('id', $id)->get();
        return view('admin.booking.return', ['booking' => $booking]);
    }

    public function checkReturn(Request $request)
    {
        $this->validate($request, [
            'paid' => 'required|gte:price',
        ]);
        
        $paid = $request->paid;
        
        return redirect('/admin/booking/verif/index/pay/'. $request->bookId .'/'. $paid);        
    }

    public function indexPay(Request $request, $id, $paid)
    {
        $booking = Booking::where('id', $id)->get();

        return view('admin.booking.payment', ['booking' => $booking, 'paid' => $paid]);
    }

    public function payTicket(Request $request)
    {    
        DB::transaction(function () {

            $bookId = Input::get('bookId');

            Payment::create([
                'price' => Input::get('price'),
                'return' => Input::get('return'),
                'paid' => Input::get('paid'),
                'booking_id' => $bookId,
            ]);
        
            Booking::find($bookId)->update(['status' => 2]);
        });

        Session::flash('alert', 'Booking paid!');
        return redirect('admin/booking');
    }
}
