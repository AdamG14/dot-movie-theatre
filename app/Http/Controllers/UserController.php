<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use App\Movie;
use App\Booking;
use App\Mail\BookingNotification;
use Carbon\Carbon;

class UserController extends Controller
{
    public function index()
    {
        $movies = Movie::orderBy('id','DESC')->get();
        return view('user.index' , ['movies' => $movies]);
    }

    public function createBooking($id)
    {
        $movies = Movie::find($id);
        // Get dates between starting date and ending date
        $from = Carbon::createFromDate($movies->ending_date);
        $to = Carbon::createFromDate($movies->starting_date);
        $idMovie = $id;

        $dates = $this->generateDateRange($to, $from);
        $seats = $this->generateAvailableSeat($idMovie, $dates);
        $now   = Carbon::now()->format('Y-m-d');

        return view('user.detail', ['movies' => $movies, 'dates' => $dates, 'seats' => $seats, 'now' => $now]);
    }

    private function generateDateRange(Carbon $start_date, Carbon $end_date)
    {
        for($date = $start_date; $date->lte($end_date); $date->addDay()) {
            $dates[] = $date->format('Y-m-d');
        }
        return $dates;
    }

    private function generateAvailableSeat($idMovie, $dates)
    {
        $row = count($dates);

        for($rows = 0; $rows < $row;){
            $seats[] = Booking::where('movie_id', $idMovie)
                                ->where('date', $dates[$rows])
                                ->get('book_seat')
                                ->sum('book_seat');
            $rows++;
        }
        return $seats;
    }

    public function storeBooking(Request $request)
    {
        // Validasi data dari form
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'movieId' => 'required',
            'seat' => 'required',
            'date' => 'required',
        ]);
        
        $availableSeat = Movie::where('id', $request->movieId)->get('seats')->sum('seats');
        $checkSeat = Booking::where('movie_id', $request->movieId)->where('date', $request->date)->get('book_seat')->sum('book_seat');
        $existSeat = $availableSeat - $checkSeat;

        if ($request->seat <= $existSeat) {
            $token = str_random(5);

            Booking::create([
                'username' => $request->username,
                'email' => $request->email,
                'phone' => $request->phone,
                'movie_id' => $request->movieId,
                'book_seat' => $request->seat,
                'date' => $request->date,
                'book_token' => $token,
            ]);
            
            $payload = [
                'id'       => $request->id,
                'email'    => $request->email,
                'username' => $request->username,
                'phone'    => $request->phone,
                'movie'    => $request->title,
                'time'     => $request->time,
                'seat'     => $request->seat,
                'date'     => $request->date,
                'token'    => $token,
            ];
            
            
            Mail::to($request->email)->send(new BookingNotification($payload)); 

            Session::flash('alert', 'Check your email to see booking detail');
            return redirect('/user');
        } else {
            Session::flash('alert', 'Seat is not available');
            return redirect('/user/detail/' . $request->movieId);
        }
        
    }

    public function indexUpcoming()
    {
        $movies = Movie::orderBy('id','DESC')->get();

        return view('user.index2' , ['movies' => $movies]);
    }

    public function showDetailUpcoming($id)
    {
        $movies = Movie::find($id);
        

        return view('user.detail2', ['movies' => $movies]);
    }

    public function indexView()
    {
        $movies = Movie::all();

        foreach($movies as $m) {
            $checkSeat[] = Booking::where('movie_id', $m->id)
                                    ->where('date', Carbon::yesterday()->format('Y-m-d'))
                                    ->where('status', 2)
                                    ->get('book_seat')
                                    ->sum('book_seat');
        }
        
        return view('user.index3' , ['movies' => $movies, 'checkSeat' => $checkSeat]);
    }

    public function indexRating()
    {
        $movies = Movie::orderBy('rate', 'DESC')->get();

        return view('user.index4' , ['movies' => $movies]);
    }
}