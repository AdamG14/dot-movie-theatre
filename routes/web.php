<?php

Route::prefix('admin')->middleware('auth')->group(function() {
    // Route::prefix('movie')->group(function() {
    //     Route::get('', 'AdminController@indexMovie');
    //     Route::get('create', 'AdminController@createMovie');
    //     Route::post('store', 'AdminController@storeMovie');
    //     Route::get('edit/{id}', 'AdminController@editMovie');
    //     Route::put('edit/update/{id}', 'AdminController@updateMovie');
    //     Route::get('delete/{id}', 'AdminController@destroyMovie');

    //     Route::resource('movie', 'AdminMovieController');
    // });
    Route::resource('movie', 'AdminMovieController');
    
    Route::prefix('booking')->group(function() {
        Route::get('', 'AdminController@indexBooking');
        Route::get('edit/{id}', 'AdminController@editBooking');
        Route::put('edit/update/{id}', 'AdminController@updateBooking');
        Route::get('delete/{id}', 'AdminController@destroyBooking');
        Route::get('verif', 'AdminController@indexVerif');
        Route::post('verif/check', 'AdminController@checkToken');
        Route::get('verif/index/{id}','AdminController@indexCheck');
        Route::post('verif/return', 'AdminController@checkReturn');
        Route::get('verif/index/pay/{id}/{paid}', 'AdminController@indexPay');
        Route::post('verif/pay', 'AdminController@payTicket');
    });
    Route::prefix('user')->group(function() {
        Route::get('register', 'AdminController@createUser');
        Route::post('store', 'AdminController@storeUser');
        Route::get('', 'AdminController@indexUser');
        Route::get('edit/{id}', 'AdminController@editUser');
        Route::put('edit/update/{id}', 'AdminController@updateUser');
        Route::get('delete/{id}', 'AdminController@destroyUser');
    });
});

Route::prefix('user')->group(function() {
    Route::get('', 'UserController@index');
    Route::get('detail/{id}', 'UserController@createBooking');
    Route::post('book/store', 'UserController@storeBooking');
    Route::get('upcoming', 'UserController@indexUpcoming');
    Route::get('views', 'UserController@indexView');
    Route::get('rating', 'UserController@indexRating');
    Route::get('detail2/{id}', 'UserController@showDetailUpcoming');
});

Route::prefix('auth')->group(function() {
    Route::get('login', 'AuthController@loginShow')->name('login');
    Route::post('login/check', 'AuthController@loginAuth');
    Route::get('logout', 'AuthController@logoutAuth');
});